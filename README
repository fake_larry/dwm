Personal patch collection
=========================
A repo with a collection of patches.

Structure
---------
Each patch is separated on its own branch. This makes it possible for me to
update the feature in case I find a bug.

    patch/6.2/togglefullscreen
     \    \   \_________________ feature
      \    \____________________ version
       \________________________ patch namespace

Documentation is separated on its own namespace.

    doc/6.2
     \   \______________________ version
      \_________________________ documentation namespace

How to rebase patches
---------------------
Make a branch named mybranch

    git checkout -b mybranch
    git reset 6.2 --hard
    git rebase patch/6.2/togglefullscreen mybranch
    git rebase patch/6.2/restart          mybranch

How to generate patches
-----------------------
Let git do the dirty work.

    git format-patch 6.2..patch/6.2/togglefullscreen 6.2..patch/6.2/restart

With bash and zsh, brace expansion can be handy.

    git format-patch 6.2..patch/6.2/{togglefullscreen,restart}

How to apply the patches
------------------------
Let git do (most of) the dirty work.

    git am *.patch

If the history is not needed.

    git apply *.patch

TODO
----
Make some nice wrapper scripts around git.

dwm - dynamic window manager
============================
dwm is an extremely fast, small, and dynamic window manager for X.


Requirements
------------
In order to build dwm you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (dwm is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install dwm (if
necessary as root):

    make clean install


Running dwm
-----------
Add the following line to your .xinitrc to start dwm using startx:

    exec dwm

In order to connect dwm to a specific display, make sure that
the DISPLAY environment variable is set correctly, e.g.:

    DISPLAY=foo.bar:1 exec dwm

(This will start dwm on display :1 of the host foo.bar.)

In order to display status info in the bar, you can do something
like this in your .xinitrc:

    while xsetroot -name "`date` `uptime | sed 's/.*,//'`"
    do
    	sleep 1
    done &
    exec dwm


Configuration
-------------
The configuration of dwm is done by creating a custom config.h
and (re)compiling the source code.
